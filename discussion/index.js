//JSON - javascript notation is a data format used by apps to store & transport data to one another; can also be used by other programming languages; files that store JSON data are saved with file extension .json;

//MiniActivity
/*
let place = {
	city: "Pasig",
	province: "Metro Manila",
	country: "Philippines"
}
console.log(place)
*/

//JSON
/*
JSON Objects:
JavaScript Object Notation
JSON - used for serializing/deserializing different data types into byte (pc of binary langiuage (1 & 0) used to represent a character
	Serialization - process of converting data into series of bytes for easier transmission / transfer of info
*/
/*
let place = {
	"city": "Pasig",
	"province": "Metro Manila",
	"country": "Philippines"
};
console.log(place);
*/

/*
DIFFERENCE
1.	JS object - only has value in quotation marks
	JSON has quotation marks for both key & value

2.  JS Object - exclusive to JS; other languages can't use JS Object files
	JSON - not exclusive to JS; other languages can also use JSON files 

*/

//JSON ARRAYS
/*
let cities = [
{
	"city": "Pasig",
	"province": "Metro Manila",
	"country": "Philippines"
},
{
	"city": "Bacoor",
	"province": "Cavite",
	"country": "Philippines"
},
{
	"city": "New York",
	"province": "New Your",
	"country": "USA"
}
];
console.log(cities); 
*/

//JSON METHODS

	//JSON Objects contain methods for parsing & converting data into stringified JSON
	//Stringified JSON - JSON Object/JS Objects converted into string to be used in other functions of the language esp JavaScript-based applications (serialize)
let batches = [
{
	"batchName": "Batch X"
},
{
	"batchName": "Batch Y"
}
];
console.log("Result from console.log");
console.log(batches);

console.log("Result from stringify method");
//stringify - used to convert JSON onjects into JSON (string); can also be used for JS Objects ( remove "" on keys)
console.log(JSON.stringify(batches));

//using stringify for JS Objects
let data = JSON.stringify ({
	name: "John",
	age: 31,
	address:{
		city: "Manila",
		country:"Philippines"
	}
});
console.log(data);

//ASSIGNMENT
/*
create userDetails var that will contain JS Object w ff properries:
fname - prompt
lname - prompt
age - prompt
address:{
	city - prompt
	country - prompt
	zipCode - prompt
}	
	log in console the converted JSON data type
*/
/*
let frstName = (prompt("Please input your first name."))
let lstName = (prompt("Please input your last name."))
let yrAge = (prompt("Please input your age."))
let yrCity = (prompt("Please input your city."))
let yrCountry = (prompt("Please input your Country."))
let yrZip = (prompt("Please input your Zipcode."))

let userDetails = {
	fname: this.fname = frstName,
	lname: this.lname = lstName,
	age: this.age = yrAge,
	address: {
		city: this.city = yrCity, 
		country: this.country = yrCountry, 
		zipCode: this.zipCode = yrZip
		}
	};

console.log(JSON.stringify(userDetails));
*/

//CONVERTING OF STRINGIFIED JSON INTO JS OBJECT
	//PARSE Method - converting JSON data into JS Object
	//info is commonly sent to application in STRINGIFIED JSON; then converted into objects; happens both for sending info to a backend app such as databases & back to frontend app such as the web pages
	//upon receiving data, JSON text can be converted into JSON/JS Object with parse method

let batchesJSON = `[
{
	"batchName": "Batch X"
},
{
	"batchName": "Batch Y"
}
]`;
console.log(batchesJSON);
console.log("Result from parse method: ");
console.log(JSON.parse(batchesJSON)); //returns array of object

let stringifiedData = `{
	"name": "John",
	"age": "31",
	"address":{
		"city": "Manila",
		"country": "Philippines"
	}
}`;
console.log(JSON.parse(stringifiedData)); //returns array of object
